﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milestone2cst117
{
    class Inventory
    {
        private List<Product> Products; //list of products in inventory
        public Inventory()
        {
            Products = new List<Product>();
        }
        public void AddProduct(Product p) //add product to list
        {
            Products.Add(p);
        }
        public bool RemoveProduct(int id) //remove product with ID
        {
            int index = Products.IndexOf(Products.Where(x => x.Id == id).FirstOrDefault());
            if (index == -1)
                return false;
            else
            {
                Products.RemoveAt(index);
                return true;
            }
        }
        public List<Product> GetProducts()//get list of products
        {
            return this.Products;
        }
        public Product getProduct(string productName) //get product by name
        {
            foreach(Product p in Products)
            {
                Console.WriteLine(p.Name);
                if (p.Name.Equals(productName))
                    return p;
            }
            return null;
        }
        public bool InStock(int id) //check for an items stock with its id
        {
            int index = Products.IndexOf(Products.Where(x => x.Id == id).FirstOrDefault());
            if (index == -1)
                return false;
            else
            {
                if (Products.ElementAt(index).Stock > 0)
                    return true;
                else
                    return false;
            }
        }
        public bool InStock(Product p) //check if an item is in stock with product object. maybe not useful but we'll see.
        {
            if (Products.Contains(p))
            {
                if (p.Stock > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
    }
}
