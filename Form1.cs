﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Forms;

namespace milestone2cst117
{
    public partial class Form1 : Form
    {
        //i have a few functions implemented that aren't used but will probably be used later. I wrote the Classes to be used on a general inv system
        // but decided to cut it down cause i didn't want to do more work for no reason. Some of the functions were kept cause they might be useful later.
        Inventory inv = new Inventory();
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //create product based on input data
            Product p = new Product(inv.GetProducts().Count, titleTextBox.Text, double.Parse(priceTextBox.Text), 1,
                GenreCheckList.CheckedItems.Cast<string>().ToList(), platformCheckList.CheckedItems.Cast<string>().ToList(), 
                ratingDropDown.SelectedItem.ToString());
            listBox1.Items.Add(p.Name);
            //add product to inventory
            inv.AddProduct(p);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clear();
            //setting fields to match selected inventory item.
            Product p = inv.getProduct(listBox1.SelectedItem.ToString());
            Console.WriteLine(p.Name);
            priceTextBox.Text = p.Price.ToString();
            titleTextBox.Text = p.Name;

            //selects appropriate checkboxes based off selection.
            ratingDropDown.SelectedItem = p.Rating;
            for (int i = 0; i < GenreCheckList.Items.Count; i++)
            {
                if (p.Genres.Contains(GenreCheckList.Items[i]))
                    GenreCheckList.SetItemChecked(i, true);
            }
            for (int i = 0; i < platformCheckList.Items.Count; i++)
            {
                if (p.Platforms.Contains(platformCheckList.Items[i]))
                    platformCheckList.SetItemChecked(i, true);
            }


        }
        private void clear() //clears the combobox and checkboxes so i can refill them with correct data.
        {
            foreach(Control ctrl in panel1.Controls)
            {
                if(ctrl is CheckedListBox)
                {
                    CheckedListBox clb = (CheckedListBox)ctrl;
                    foreach (int checkedItemIndex in clb.CheckedIndices)
                        clb.SetItemChecked(checkedItemIndex, false);
                }
                if(ctrl is ComboBox)
                {
                    ComboBox cb = (ComboBox)ctrl;
                    cb.SelectedIndex = 0;
                }
            }
        }
    }
}
