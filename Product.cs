﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milestone2cst117
{
    class Product
    {
        private int id;
        private String name;
        private double price;
        private int stock;
        private List<String> genres;
        private List<String> platforms;
        private String rating;
        
        public Product(int id, String Name, double price, int stock, List<String> Genres, List<String> Platforms, String Rating)
        {
            this.id = id;
            this.name = Name;
            this.genres = Genres;
            this.platforms = Platforms;
            this.rating = Rating;
            this.price = price;
            this.stock = stock;
        }

        //vs generated getters and setters, should probably add error handling
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; } 
        public int Stock { get => stock; set => stock = value; }
        public List<string> Genres { get => genres; set => genres = value; }
        public List<string> Platforms { get => platforms; set => platforms = value; }
        public string Rating { get => rating; set => rating = value; }
    }
}
